<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Flor2u</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/script.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

</head>
<body>
<div id="app">
    <a href="https://new.dreammoto.ru">ТЫЦ</a>
    <header>
        <div class="container">
            <ul class="nav nav-pills">
                <li><a href="{{ route('weather') }}">Погода</a></li>
                <li><a href="{{ route('orders') }}">Заказы</a></li>
                <li><a href="{{ route('products') }}">Продукты</a></li>
            </ul>
        </div>
    </header>

    <div class="container">

        @yield('content')

    </div>

</div>
</body>
</html>
