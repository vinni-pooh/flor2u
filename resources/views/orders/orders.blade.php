@extends('app')

@section('content')

    <h1>Заказы</h1>

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Партнер</th>
            <th>Состав заказа</th>
            <th>Сумма заказа</th>
            <th>Статус</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($orders as $order)
            <tr>
                <th>
                    <a href="{{ route('order_edit', $order->id) }}">
                        <b>{{ $order->id }}</b>
                    </a>
                </th>

                <td>
                    {{ $order->partner->name }}
                </td>

                <td>
                    @foreach ($order->order_products as $order_product)
                        {{ $order_product->product->name }} - {{ $order_product->quantity }} шт<br>
                    @endforeach
                </td>

                <td>
{{--                    Не совсем изящно, но нет времени думать над этим--}}
                    {{ $order->sum_order_products[0]->sum }}
                </td>

                <td>
                    {{ $order->status }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="row">
        <div class="col-12 mt-3">
            {{ $orders->links() }}
        </div>
    </div>

@endsection