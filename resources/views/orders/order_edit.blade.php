@extends('app')

@section('content')

    <h1>Редактирование заказа № {{ $current_order->id }}</h1>

    <div class="row">
        <div class="col-md-6">

            {{ Form::open(array('action'=>'OrderController@order_save')) }}

            {{ Form::hidden('id',$current_order->id,array('id'=>'id', 'class'=>'form-control')) }}

            {{ Form::label('client_email', 'E-Mail') }}
            {{ Form::text('client_email',$current_order->client_email,array('id'=>'client_email', 'class'=>'form-control', 'required')) }}
            <br>

            {{ Form::label('partner_id', 'Партнер') }}
            {{ Form::select('partner_id', $partners, $current_order->partner_id, array('class'=>'form-control', 'required')) }}
            <br>

            <div class="row">
                <div class="col-md-8">
                    <b>Состав заказа</b><br>
                    @foreach ($current_order->order_products as $order_product)
                        {{ $order_product->product->name }} - {{ $order_product->quantity }} шт, {{ $order_product->price }} руб<br>
                    @endforeach
                    <br>
                </div>

                <div class="col-md-4">
                    <b>Сумма заказа</b><br>
                    {{ $current_order->sum_order_products[0]->sum }}
                </div>
            </div>

            {{ Form::label('status', 'Статус заказа') }}
            {{ Form::text('status',$current_order->status,array('id'=>'', 'class'=>'form-control', 'required')) }}
            <br>

            {{ Form::submit('Сохранить') }}  <span class="saved-alert">{{{ $saved or '' }}}</span>

            {{ Form::close() }}

        </div>
    </div>

@endsection