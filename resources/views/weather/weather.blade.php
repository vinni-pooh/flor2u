@extends('app')

@section('content')

    <h1>Погода</h1>

    <p>У меня нет токена для работы с API Yandex, поэтому в качестве примера работа с API VK</p>
    <p>Получение описания группы с фотографией</p>

    <div class="row">
        <div class="col-md-2">
            <img src="{{ $response['photo_200'] }}" alt="" class="img-responsive">
        </div>
        <div class="col-md-4">
            <h4>{{ $response['name'] }}</h4>
        </div>
    </div>

@endsection