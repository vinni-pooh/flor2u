@extends('app')

@section('content')

    <h1>Продукты</h1>

    <p>
        Поле редактирования цены реализовано в виде однофайлового компонента на Vue.js.<br>
        Сохранение соответственно через Ajax
    </p>

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Продукт</th>
            <th>Поставщик</th>
            <th>Цена</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($products as $product)
            <tr>
                <th>
                    {{ $product->id }}
                </th>

                <td>
                    {{ $product->name }}
                </td>

                <td>
                    {{ $product->vendor->name }}
                </td>

                <td>
                    <edit-price
                            :product="{{ $product }}">
                    </edit-price>
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="row">
        <div class="col-12 mt-3">
            {{ $products->links() }}
        </div>
    </div>

@endsection