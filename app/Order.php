<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //Продукты заказа
    public function order_products()
    {
        return $this->hasMany(OrderProduct::class, 'order_id', 'id');
    }

    //Партнеры
    public function partner()
    {
        return $this->belongsTo(Partner::class, 'partner_id', 'id');
    }

    //Сумма заказа (из задачи непонятно, цена указана за единицу или за общее количество. Будем считать, что за все)
    public function sum_order_products()
    {
        return $this->hasMany(OrderProduct::class, 'order_id', 'id')
            ->groupBy('order_id')
            ->selectRaw('sum(price) as sum, order_id');
    }
}
