<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    //Продукты
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
