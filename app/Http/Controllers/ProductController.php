<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {

        $products = Product::with('vendor')
            ->orderBy('name')
            ->paginate(25);

        return view('products.products', [
            'products' => $products,
        ]);

    }

    public function price_save(Request $request)
    {

        $product = Product::find($request->id);

        $product->price = $request->price;

        try {

            $product->save();
            $response['success'] = true;
            return $response;

        } catch (\Exception $ex) {

            $response['success'] = false;
            return $response;

        }


    }
}
