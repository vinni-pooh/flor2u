<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class WeatherController extends Controller
{
    public function index()
    {

        $url = 'https://api.vk.com/method/groups.getById?group_id=17821251&access_token=07870c5a07870c5a07870c5a0207e5166d0078707870c5a5d3ba3a5978481adb581027c&v=5.74';
        $response = collect(json_decode(file_get_contents($url), true)['response'][0]);

        return view('weather.weather', [
            'response'           => $response,
        ]);

    }
}
