<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderProduct;
use App\Partner;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {

        $orders = Order::with('partner')
            ->with('order_products.product')
            ->with('sum_order_products')
            ->orderBy('id')
            ->paginate(25);

        return view('orders.orders', [
            'orders' => $orders,
        ]);

    }

    public function order_edit(Request $request)
    {
        $current_order = Order::where('id', $request->route('id'))
            ->with('partner')
            ->with('order_products.product')
            ->with('sum_order_products')
            ->first();

        $partners = Partner::get()->pluck('name', 'id');

        $saved = $request->saved;

        return view('orders.order_edit', [
            'current_order' => $current_order,
            'partners' => $partners,
            'saved' => $saved,
        ]);

    }

    public function order_save(Request $request)
    {

        $order = Order::find($request->id);

        $order->client_email = $request->client_email;
        $order->partner_id = $request->partner_id;
        $order->status = $request->status;

        $order->save();

        //перенаправление на страницу редактирования с уведомлением о сохранении
        return redirect()->route('order_edit', [$request->id, 'saved'=>'Заказ сохранен']);

    }
}
