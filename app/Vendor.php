<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    //Продукты
    public function products()
    {
        return $this->hasMany(Product::class, 'product_id', 'id');
    }
}
