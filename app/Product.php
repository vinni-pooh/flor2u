<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //Продукты заказа
    public function order_products()
    {
        return $this->hasMany(OrderProduct::class, 'product_id', 'id');
    }

    //Поставщик
    public function vendor()
    {
        return $this->belongsTo(Vendor::class, 'vendor_id', 'id');
    }
}
