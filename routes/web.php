<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Погода
Route::get('/weather', 'WeatherController@index')->name('weather');

//Заказы
Route::group(['prefix' => 'orders'], function () {
    Route::get('/', 'OrderController@index')->name('orders');
    Route::post('/order_save', 'OrderController@order_save')->name('order_save');
    Route::get('/{id}', 'OrderController@order_edit')->name('order_edit');
});

//Каталог продуктов
Route::get('/product', 'ProductController@index')->name('products');

//Сохранение цены
Route::post('/price_save', 'ProductController@price_save');